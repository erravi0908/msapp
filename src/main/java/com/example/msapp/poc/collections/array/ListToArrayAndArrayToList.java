package com.example.msapp.poc.collections.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListToArrayAndArrayToList {
    public static void main(String[] args) {

         List<Integer> list=new ArrayList<Integer>();

         list.add(100);
        Integer[] arr=new Integer[list.size()];
        //create an array and pass to this list.toArray(empty array)
        list.toArray(arr);

        System.out.println(arr.length);

        for (Integer item: arr) {
            System.out.println(item);
        }

        // Array To List

        List<Integer> newList=new ArrayList<Integer>(Arrays.asList(arr));


        System.out.println(newList.size());

    }

}

