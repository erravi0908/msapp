package com.example.msapp.poc.collections.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArraysAsList {
    public static void main(String[] args) {

        Integer[] arr={1,2,3,4,5};

        List<Integer> list=new ArrayList<>(Arrays.asList(arr));


        for (Integer i:
             list) {
            System.out.println(i);

        }

    }
}
