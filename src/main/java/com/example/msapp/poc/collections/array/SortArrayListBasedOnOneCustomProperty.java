package com.example.msapp.poc.collections.array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortArrayListBasedOnOneCustomProperty {

    static class Employee
    {
        int age;
        String name;

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Employee{" +
                    "age=" + age +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    public static void main(String[] args) {


        List<Employee> empList=new ArrayList<>();

        Employee e1=new Employee();
        e1.setAge(22);
        e1.setName("Ravi A");

        Employee e2=new Employee();
        e2.setAge(12);
        e2.setName("Ravi B");

        Employee e3=new Employee();
        e3.setAge(2);
        e3.setName("Ravi C");

        empList.add(e1);empList.add(e2);empList.add(e3);


        System.out.println(empList);
        empList=sortByAge(empList);

        System.out.println("\n\n\n");
        System.out.println(empList);

        empList=sortByName(empList);
        System.out.println("\n\n\n" + "By Name Sorted Below");
        System.out.println(empList);
    }

    public static List<Employee> sortByAge(List<Employee> list)
    {
        Collections.sort(list,(l1,l2)->(((Integer)l1.getAge()).compareTo(((Integer) l2.getAge()))));

        return list;
    }

    public static List<Employee> sortByName(List<Employee> list)
    {
        Collections.sort(list,(l1,l2)->((l1.getName()).compareTo((l2.getName()))));

        return list;
    }





}
