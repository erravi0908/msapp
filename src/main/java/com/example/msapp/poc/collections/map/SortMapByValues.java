package com.example.msapp.poc.collections.map;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class SortMapByValues {

    public static void main(String[] args) {

        Map<Integer,Integer> map=new ConcurrentHashMap<>();

        map.put(10,1);map.put(2,200);map.put(1,100);map.put(5,2);


       List<Map.Entry<Integer,Integer>> list=new ArrayList<>(map.entrySet());


        list=sortList(list);


        Map<Integer,Integer> sortedMap=new LinkedHashMap<>();

        for(Map.Entry<Integer,Integer> m:list)
        {
         sortedMap.put(m.getKey(),m.getValue()) ;
        }


        System.out.println("Sorted Map---->>" + sortedMap);

    }

    public static List<Map.Entry<Integer,Integer>> sortList(List<Map.Entry<Integer,Integer>> list)
    {

        System.out.println(list);

        Collections.sort(list,(l1,l2)->(l1.getValue().compareTo(l2.getValue())));

        return  list;
    }



}


/*
public static void main(String[] args) {
        LinkedHashMap<Integer,Integer> map=new LinkedHashMap<>();

        map.put(1,100);map.put(2,25);map.put(3,4);map.put(5,1);

        //get keys of the map
        List<Integer> keys=new ArrayList<>(map.keySet());

        // get values of the map
        List<Integer> values=new ArrayList<>(map.values());

        // get Enteries of the map

        System.out.println("Before Sort Map List====>" +map);

        List<Map.Entry<Integer,Integer>> sortMapList=sortMap(map);
        LinkedHashMap<Integer,Integer> sortedMap= new LinkedHashMap<>();

        System.out.println("Sort Map List====>" +sortMapList);

        for (Map.Entry<Integer,Integer> entry: sortMapList) {

            System.out.println("" + entry.getKey()  +"\t value \t "+ entry.getValue());
            sortedMap.put(entry.getKey(),entry.getValue());
        }

        System.out.println("Sorted Map \t" + sortedMap);
    }

    public static  List<Map.Entry<Integer,Integer>> sortMap(Map<Integer,Integer> map)
    {
            List<Map.Entry<Integer,Integer>> mapAsList= new ArrayList<>(map.entrySet());


            Collections.sort(mapAsList,(l1,l2)->(l1.getValue().compareTo(l2.getValue())));

            System.out.println("Map as List After Sorting--->"+mapAsList);

            return  mapAsList;

    }

 */