package com.example.msapp.poc.collections.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SortArrayList {

    public static void main(String[] args) {

        Integer[] arr={1,2222,3,2,555};

        List<Integer> list=new ArrayList<>(Arrays.asList(arr));

        System.out.println(list);

        Collections.sort(list,(l1,l2)->l1.compareTo(l2));

        System.out.println(list);


    }
}
