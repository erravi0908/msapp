package com.example.msapp.poc.collections.set;

import java.util.*;
import java.util.stream.Collectors;

public class ConvertArrayToSet {

    public static void main(String[] args) {

    Integer[] arr={1,2,3,4,5};

    // First approach
      List<Integer>  list= Arrays.stream(arr).collect(Collectors.toList());
      Set<Integer> set=Arrays.stream(arr).collect(Collectors.toSet());

    // 2nd Approach
        Set<Integer> set2=new HashSet<>(Arrays.asList(arr));

        List<Integer> list2=new ArrayList<>(Arrays.asList(arr));

        System.out.println(set2.size() + " " +list2.size());


    }
}
