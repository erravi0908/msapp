package com.example.msapp.poc.collections.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class JoinTwoList {

    public static void main(String[] args) {
//        Integer[] arr1={1,2,3,4,5};
//        Integer[] arr2={11,22,33,44,55};

        String[] arr1={"A","B"};
        String[] arr2={"C","D"};


        List<String> list1= Arrays.asList(arr1);
        List<String> list2=Arrays.asList(arr2);


        List<String> combinedList = new ArrayList<String>();

        combinedList.addAll(list1);

        combinedList.addAll(list2);

        System.out.println("combinedList = " + combinedList);

        // List created using Arrays.asList() cant be used for combining
        // and will thorw expection if we try to call addAll() on
        // combined2 list
        List<String> combined2=Arrays.asList();

       // it will throw runtime exception UnsupportedOperationException
        // combined2.addAll(combinedList);

    }
}
