package com.example.msapp.poc.collections.array;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ConvertHashMapToList {

    public static void main(String[] args) {

        //Map to List
        Map<Integer, Integer> map = new ConcurrentHashMap<>();

        map.put(1, 11);
        map.put(2, 22);

        //Get keys
        List<Integer> keysList = new ArrayList<>(map.keySet());
        // Get Values
        List<Integer> valuesList = new ArrayList(map.values());


        //-----------Using Steam.collect(Collectors.toList)-----

        List<Integer> keyList = map.keySet().stream().collect(Collectors.toList());

        List<Integer> valuesList2 = map.values().stream().collect(Collectors.toList());




    }
}
