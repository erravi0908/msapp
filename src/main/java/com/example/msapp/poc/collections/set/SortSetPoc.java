package com.example.msapp.poc.collections.set;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class SortSetPoc {

    public static void main(String[] args) {
        //Array to List
        Integer[] arr={12,3,4,5};
        List<Integer> list=new ArrayList<>(Arrays.asList(arr));

        System.out.println(list);

        //List to Array
        Integer[] arr2=new Integer[list.size()];

        list.toArray(arr2);

        System.out.println(arr2.length);

        //List to Set
        Set<Integer> set=new HashSet<>(list);

        //Set to List
        List<Integer> list2=new ArrayList<>(set);


        // Use Map, show its keys, values, sort its values;
        Map<Integer,Integer> map=new ConcurrentHashMap<>();

        map.put(2,11);map.put(0,0);map.put(3,1111);

        System.out.println(map);


       Set<Integer> keys=map.keySet();

       Set<Map.Entry<Integer,Integer>> entries= map.entrySet();

       List<Map.Entry<Integer,Integer>> listsort=new ArrayList<>(entries);

        listsort=sort(listsort);


        LinkedHashMap<Integer,Integer> sortedMap=new LinkedHashMap<>();


        for(Map.Entry<Integer,Integer> e:listsort)
        {
            sortedMap.put(e.getKey(),e.getValue());
        }


    }

    public static List<Map.Entry<Integer,Integer>> sort(List<Map.Entry<Integer,Integer>> list)
    {
        Collections.sort(list,(l1,l2)->(l1.getValue().compareTo(l2.getValue())));

        return list;
    }




}
/*
    public static void main(String[] args) {
        //Array To List
        Integer[] arr={1,23,3,4,5};

        List<Integer> list=new ArrayList<>(Arrays.asList(arr));

        System.out.println(list);

        // List to Array
        Integer[] arr2=new Integer[list.size()];

        list.toArray(arr2);

        System.out.println("arr2.length = " + arr2.length);


        // List to Set
        Set<Integer> set=new HashSet<>(list);


        // Set to List
        List<Integer> newList=new ArrayList<>(set);

        newList.add(-1);newList.add(0);

        System.out.println("newList = " + newList);

        Collections.sort(newList,(l1,l2)->(l1.compareTo(l2)));

        System.out.println("newList = " + newList);


       // Map creation, extract map keys, extract map values as List and Sort of its values
        Map<Integer,Integer>  map=new LinkedHashMap();

        map.put(1,100);map.put(2,20);map.put(3,19);map.put(4,10);

        Set<Integer> keys=map.keySet();

        Set<Map.Entry<Integer,Integer>> valuesSet=map.entrySet();

        List<Map.Entry<Integer,Integer>> valuesList=new ArrayList<>(valuesSet);

        System.out.println("Before list sort \t" + valuesList);
        valuesList=sortedValues(valuesList);

        System.out.println("After list sort \t" + valuesList);


        LinkedHashMap<Integer,Integer> sortedMap=new LinkedHashMap<>();

        for (Map.Entry<Integer,Integer> entry:
             valuesList) {

            sortedMap.put(entry.getKey(),entry.getValue());

        }


        System.out.println("Soreted Map \t" + sortedMap);

    }

    public static List<Map.Entry<Integer,Integer>>  sortedValues(List<Map.Entry<Integer,Integer>>  list)
    {

        Collections.sort(list,(l1,l2)->(l1.getValue().compareTo(l2.getValue())));

        return list;
    }

 */