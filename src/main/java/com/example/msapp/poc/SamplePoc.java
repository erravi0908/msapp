package com.example.msapp.poc;

import java.util.Scanner;

class SamplePoc
{

    public static void main(String[] args) {

//        //showFibonaci();
//        showLeapYear(2000);
//        showLeapYear(1700);
//        showLeapYear(1300);
//        showLeapYear(1400);

         fiboRecursion(0,1,5);
         showFibonaciForUpToN_Number(5);
         palindRome(1210);
    }

    public static void showFibonaci()
    {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter No ");
        int nth=sc.nextInt();
        int t1=0;int t2=1;int nextTerm=t1+t2;
         System.out.print(t1 +"\t" + t2);
        while(nextTerm<=nth)
        {
            System.out.print("\t"+nextTerm);
            t1=t2;
            t2=nextTerm;
            nextTerm=t1+t2;
        }

    }
    public static void showFibonaciForUpToN_Number(int no)
    {

        int t1=0;int t2=1;int nextTerm=t1+t2;
        System.out.print(t1 +"\t" + t2);
        while(nextTerm<=no)
        {
            System.out.print("\t"+nextTerm);
            t1=t2;
            t2=nextTerm;
            nextTerm=t1+t2;
        }

    }

    public static void showLeapYear(int year)
    {
        if(year%4==0 && year%400==0)
        {
            System.out.println("Leap Year \t" + year);
        }else
        {
            System.out.println("Not Leap Year \t" + year);
        }



    }


    public static void fiboRecursion(int t1,int t2,int nth)
    {
        int nextTerm=t1+t2;
        System.out.println(" \t" + nextTerm);
        if(nextTerm>nth)
        {
            return;
        }
        else
        {
            t1=t2;
            t2=nextTerm;
            fiboRecursion(t1,t2,nth);
        }

    }


    public static void palindRome(int no)
    {
        int rev=0;
        int originalNo=no;
        while(no>0)
        {
            int rem=no%10;
            rev=rev*10+rem;
            no=no/10;

        }
        if(rev==originalNo)
        {
            System.out.println(rev +" is palindrome");
        }else
        {
            System.out.println(rev +" is not palindrome");

        }

    }


}