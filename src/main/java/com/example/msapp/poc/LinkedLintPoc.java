package com.example.msapp.poc;

public class LinkedLintPoc {

    Node head;
    static class Node
    {
        int value;
        Node next;

        Node(int data)
        {
            value=data;
            next=null;
        }
    }

    public static void main(String[] args) {
        LinkedLintPoc ll=new LinkedLintPoc();

        ll.head=new Node(10);

        //create a new Node
        Node node2=new Node(20);
        //create a 3rd Node
        Node node3=new Node(30);

        // add the nodes to the linkedlist
        ll.head.next=node2;
        node2.next=node3;


        while(ll.head!=null)
        {
            System.out.println( "\t" + ll.head.value);
            ll.head=ll.head.next;

        }








    }
}
