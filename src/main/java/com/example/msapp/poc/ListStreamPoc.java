package com.example.msapp.poc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListStreamPoc {

    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        int[] arr={1,2,3,4,5};

       System.out.println("Sum of Array via Stream"+Arrays.stream(arr).sum());
        Integer[] arr2={1,2,3,4,5};
       // convert Array to List
       List<Integer> l=Arrays.asList(arr2);

       System.out.println(l);



    }
}
