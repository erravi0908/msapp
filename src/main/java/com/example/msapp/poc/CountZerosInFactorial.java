package com.example.msapp.poc;

import java.util.Scanner;

public class CountZerosInFactorial {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int no=sc.nextInt();
        // Approach
        // count pairs of (2,5) that no of zeros will be there
        int res=0;
        for(int i=5;i<=no;i=i*5)
        {
            res=res+no/i;
        }

        System.out.println(res);
    }
}
