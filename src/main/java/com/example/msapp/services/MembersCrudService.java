package com.example.msapp.services;

import com.example.msapp.models.Members;
import com.example.msapp.repos.MembersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class MembersCrudService {
    @Autowired
    MembersRepo membersRepo;
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    MongoOperations mongoOperations;

    public Members findMember(Integer id)
    {
        if  (membersRepo.findById(id).isPresent())
        {
            return  membersRepo.findById(id).get();
        }else
        {
            return null;
        }
    }

    public Members saveMember(Members members)
    {
       return membersRepo.save(members);
    }
}
