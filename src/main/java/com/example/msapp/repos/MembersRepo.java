package com.example.msapp.repos;

import com.example.msapp.models.Members;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MembersRepo extends MongoRepository<Members,Integer> {
}
