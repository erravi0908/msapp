package com.example.msapp.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.File;

@Document("members")
public class Members {
        @Id
        @Field("_id")
        private String _id;

        @Field("fullName")
        private String fullName;

    @Field("image")
    private File image;

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "Members{" +
                "_id=" + _id +
                ", fullName='" + fullName + '\'' +
                '}';
    }
}