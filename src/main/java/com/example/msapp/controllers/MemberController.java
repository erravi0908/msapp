package com.example.msapp.controllers;

import com.example.msapp.models.Members;
import com.example.msapp.services.MembersCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/")
public class MemberController {

    @Autowired
    MembersCrudService membersCrudService;
    @GetMapping(name = "/",produces = "application/json")
    public Members getId()
    {
        System.out.println("controller" +membersCrudService.findMember(1));

        return  membersCrudService.findMember(1);
    }
    @PostMapping(value = "/create/member",
           // consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            consumes = "multipart/form-data",
            produces = "application/json"
    ) public Members postMember( Members members)
    {

        Members savedMembers= membersCrudService.saveMember(members);
        return  savedMembers;
    }


}
